/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rannilaitc.judoshiai.helper.dao;

import com.rannilaitc.judoshiai.helper.domain.Category;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jrannila
 */
public class CategoryDao {
    
    private final Connection conn;
    
    public CategoryDao() {
        conn = ConnectionUtil.getConnection();
    }
    
    public List<Category> getAll() {
        List<Category> list = new ArrayList<>();
        
        String sql = "select * from categories";
        
        Statement st = null;
        ResultSet rs = null;
        try {
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            while(rs.next()) {
                list.add(mapRow(rs));
            }
            return list;
        }
        catch(SQLException e) {
            e.printStackTrace();
            return new ArrayList();
        }
        finally {
            try { rs.close(); } catch (Exception e){}
            try { st.close(); } catch (Exception e){}
        }
    }
    
    private Category mapRow(ResultSet rs) throws SQLException {
        Category c = new Category();
        c.setCategory(rs.getString("category"));
        c.setDeleted(rs.getInt("deleted"));
        c.setGroup(rs.getInt("group"));
        c.setIndex(rs.getInt("index"));
        c.setNumcomp(rs.getInt("numcomp"));
        c.setPos1(rs.getInt("pos1"));
        c.setPos2(rs.getInt("pos2"));
        c.setPos3(rs.getInt("pos3"));
        c.setPos4(rs.getInt("pos4"));
        c.setPos5(rs.getInt("pos5"));
        c.setPos6(rs.getInt("pos6"));
        c.setPos7(rs.getInt("pos7"));
        c.setPos8(rs.getInt("pos8"));
        c.setSystem(rs.getInt("system"));
        c.setTable(rs.getInt("table"));
        c.setTatami(rs.getInt("tatami"));
        c.setWishsys(rs.getInt("wishsys"));
        return c;
    }
}
