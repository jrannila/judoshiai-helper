/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rannilaitc.judoshiai.helper.dao;

import com.rannilaitc.judoshiai.helper.domain.Competitor;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jrannila
 */
public class CompetitorDao {

    private final Connection conn;

    public CompetitorDao() {
        this.conn = ConnectionUtil.getConnection();
    }

    public List<Competitor> getAllActive() {
        List<Competitor> list = new ArrayList<>();

        String sql = "select * from competitors where visible=1";

        Statement st = null;
        ResultSet rs = null;

        try {
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {
                list.add(mapRow(rs));
            }
            
            return list;
        }    
        catch(Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
        finally {
            try { rs.close(); } catch (Exception e){}
            try { st.close(); } catch (Exception e){}
        }
    }
    
    private Competitor mapRow(ResultSet rs) throws SQLException {
        Competitor c = new Competitor();
        c.setBelt(rs.getInt("belt"));
        c.setBirthyear(rs.getInt("birthyear"));
        c.setCategory(rs.getString("category"));
        c.setClub(rs.getString("club"));
        c.setClubseeding(rs.getInt("clubseeding"));
        c.setCoachId(rs.getString("coachid"));
        c.setComment(rs.getString("comment"));
        c.setCountry(rs.getString("country"));
        c.setDeleted(rs.getInt("deleted"));
        c.setFirst(rs.getString("first"));
        c.setId(rs.getString("id"));
        c.setIndex(rs.getInt("index"));
        c.setLast(rs.getString("last"));
        c.setRegcategory(rs.getString("regcategory"));
        c.setSeeding(rs.getInt("seeding"));
        c.setVisible(rs.getInt("visible"));
        c.setWeight(rs.getInt("weight"));
        return c;
    }
}
