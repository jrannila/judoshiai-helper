/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rannilaitc.judoshiai.helper.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author jrannila
 */
public class ConnectionUtil {
    
    private static Connection c;
    
    public static void init(String path) throws SQLException {
        try {
        Class.forName("org.sqlite.JDBC");
        c = DriverManager.getConnection("jdbc:sqlite:/"+path);
        }
        catch (Exception e) {
            throw new IllegalArgumentException("Could't initialize database connection", e);
        }
    }
    
    public static Connection getConnection() {
        if(c == null) {
            throw new IllegalStateException("Connection not initialized yet. Call Connection.init(String path) first.");
        }
        else {
            return c;
        }
    }
    
    public static void closeConnection() {
        if(c != null) {
            try { c.close(); } catch (Exception e){}
        }
    }
}
