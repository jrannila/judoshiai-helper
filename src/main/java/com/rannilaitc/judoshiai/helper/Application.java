package com.rannilaitc.judoshiai.helper;

import com.rannilaitc.judoshiai.helper.dao.CategoryDao;
import com.rannilaitc.judoshiai.helper.dao.ConnectionUtil;
import com.rannilaitc.judoshiai.helper.domain.Category;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jrannila
 */
public class Application {
    
    private void init() throws Exception {
        ConnectionUtil.init("/home/jrannila/projektit/omat/Judo/dev/judo-am-2016.shi");
    }
    
    private void test() {
        CategoryDao catDao = new CategoryDao();
        List<Category> list = catDao.getAll();
        list.stream().forEach((c) -> {
            System.out.println(c.getCategory());
        });
        
    }
    
    public static void main(String[] args) {
        Application a = new Application();
        
        try {
            a.init();
            a.test();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            ConnectionUtil.closeConnection();
        }
        
        
    }
}
