/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rannilaitc.judoshiai.helper.domain;

/**
 *
 * @author jrannila
 */
public class Category {
    
    private int index;
    private String category;
    private int tatami;
    private int deleted;
    private int group;
    private int system;
    private int numcomp;
    private int table;
    private int wishsys;
    private int pos1;
    private int pos2;
    private int pos3;
    private int pos4;
    private int pos5;
    private int pos6;
    private int pos7;
    private int pos8;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getTatami() {
        return tatami;
    }

    public void setTatami(int tatami) {
        this.tatami = tatami;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getSystem() {
        return system;
    }

    public void setSystem(int system) {
        this.system = system;
    }

    public int getNumcomp() {
        return numcomp;
    }

    public void setNumcomp(int numcomp) {
        this.numcomp = numcomp;
    }

    public int getTable() {
        return table;
    }

    public void setTable(int table) {
        this.table = table;
    }

    public int getWishsys() {
        return wishsys;
    }

    public void setWishsys(int wishsys) {
        this.wishsys = wishsys;
    }

    public int getPos1() {
        return pos1;
    }

    public void setPos1(int pos1) {
        this.pos1 = pos1;
    }

    public int getPos2() {
        return pos2;
    }

    public void setPos2(int pos2) {
        this.pos2 = pos2;
    }

    public int getPos3() {
        return pos3;
    }

    public void setPos3(int pos3) {
        this.pos3 = pos3;
    }

    public int getPos4() {
        return pos4;
    }

    public void setPos4(int pos4) {
        this.pos4 = pos4;
    }

    public int getPos5() {
        return pos5;
    }

    public void setPos5(int pos5) {
        this.pos5 = pos5;
    }

    public int getPos6() {
        return pos6;
    }

    public void setPos6(int pos6) {
        this.pos6 = pos6;
    }

    public int getPos7() {
        return pos7;
    }

    public void setPos7(int pos7) {
        this.pos7 = pos7;
    }

    public int getPos8() {
        return pos8;
    }

    public void setPos8(int pos8) {
        this.pos8 = pos8;
    }
    
}
